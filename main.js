import Vue from 'vue'
import App from './App.vue'

import VueRouter from 'vue-router'
import VueTextareaAutosize from 'vue-textarea-autosize'
import RouterSync from 'vue-router-sync'

Vue.use(VueRouter)
Vue.use(RouterSync)
Vue.use(VueTextareaAutosize)

import AppNavbar from './components/Navbar'
import AppFlowchart from './components/Flowchart'
import AppSpreadsheet from './components/Spreadsheet'
import AppDocumentation from './components/Documentation'

Vue.component('AppNavbar', AppNavbar);
Vue.component('AppFlowchart', AppFlowchart);
Vue.component('AppSpreadsheet', AppSpreadsheet);
Vue.component('AppDocumentation', AppDocumentation);

const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: AppFlowchart},
        { path: '/spreadsheet', component: AppSpreadsheet },
        { path: '/documentation', component: AppDocumentation }
    ],
    linkExactActiveClass: 'is-active'
})

// flowchart code
function makePort(name, spot, output, input) {
    // the port is basically just a small circle that has a white stroke when it is made visible
    return $(go.Shape, "Circle",
             {
                fill: "transparent",
                stroke: null,
                desiredSize: new go.Size(5, 5),
                alignment: spot, alignmentFocus: spot,
                portId: name,
                fromSpot: spot, toSpot: spot,
                fromLinkable: output, toLinkable: input,
                cursor: "pointer"
             });
  };

function nodeStyle() {
    return [
      new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
      {
        locationSpot: go.Spot.Center,

        mouseEnter: function (e, obj) { showPorts(obj.part, true); },
        mouseLeave: function (e, obj) { showPorts(obj.part, false); }
      }
    ];
};

function showPorts(node, show) {
    var diagram = node.diagram;
    if (!diagram || diagram.isReadOnly || !diagram.allowLink) return;
    node.ports.each(function(port) {
        port.stroke = (show ? "white" : null);
      });
};

function showLinkLabel(e) {
    var label = e.subject.findObject("LABEL");
    if (label !== null) label.visible = (e.subject.fromNode.data.figure === "Diamond");
};

var lightText = 'whitesmoke';
var $ = go.GraphObject.make;
Vue.component("diagram", {
    template: "<div></div>",
    props: ["modelData"],
    data: function() {
        return {
            diagram: null
        }
    },
    mounted: function() {
        var self = this;
        const myDiagram =
        $(go.Diagram, "myDiagramDiv",
        {
          initialContentAlignment: go.Spot.Center,
          allowDrop: true,
          "LinkDrawn": showLinkLabel,
          "LinkRelinked": showLinkLabel,
          scrollsPageOnFocus: false,
          "undoManager.isEnabled": true,
          "ModelChanged": function(e) { self.$emit("model-changed", e); },
          "ChangedSelection": function(e) { self.$emit("changed-selection", e); },
          "animationManager.isEnabled": false
        });
        myDiagram.nodeTemplateMap.add ("",
        $(go.Node, "Spot", nodeStyle(),
            {resizable: true},
            // the main object is a Panel that surrounds a TextBlock with a rectangular Shape
            $(go.Panel, "Auto",
            $(go.Shape, "Rectangle",
                { fill: "#94a0b2", stroke: null },
                
                new go.Binding("figure", "figure")),
            $(go.TextBlock,
                {
                font: "font-size: 1rem",
                stroke: lightText,
                margin: 8,
                maxSize: new go.Size(90, NaN),
                wrap: go.TextBlock.WrapFit,
                editable: true,
                
                },
                new go.Binding("text").makeTwoWay())
            ),
            // four named ports, one on each side:
            makePort("T", go.Spot.Top, true, true),
            makePort("L", go.Spot.Left, true, true),
            makePort("R", go.Spot.Right, true, true),
            makePort("B", go.Spot.Bottom, true, true)
        ));
        myDiagram.nodeTemplateMap.add ("Start",
        $(go.Node, "Spot", nodeStyle(),
            $(go.Panel, "Auto",
            $(go.Shape, "Ellipse",
                { minSize: new go.Size(80, 40), fill: "#26A65B", stroke: null }),
            $(go.TextBlock, "Start",
                {stroke: lightText },
                new go.Binding("text"))
            ),
            // three named ports, one on each side except the top, all output only:
            makePort("L", go.Spot.Left, true, false),
            makePort("R", go.Spot.Right, true, false),
            makePort("B", go.Spot.Bottom, true, false)
        )
        );
        myDiagram.nodeTemplateMap.add ("End",
        $(go.Node, "Spot", nodeStyle(),
            $(go.Panel, "Auto",
            $(go.Shape, "Ellipse",
                { minSize: new go.Size(80, 40), fill: "#EF4836", stroke: null }),
            $(go.TextBlock, "End",
                { stroke: lightText },
                new go.Binding("text"))
            ),
            // three named ports, one on each side except the bottom, all input only:
            makePort("T", go.Spot.Top, false, true),
            makePort("L", go.Spot.Left, false, true),
            makePort("R", go.Spot.Right, true, true)
        )
        );
        myDiagram.nodeTemplateMap.add("Comment",
        $(go.Node, "Spot", nodeStyle(),
            $(go.Shape, "File",
            { maxSize: new go.Size(80, 100), fill: "#F5D76E", stroke: null }),
            $(go.TextBlock, "Yes",
                {
                    font: "font-size: 1rem !important",
                    // stroke: lightText,
                    margin: 8,
                    maxSize: new go.Size(70, NaN),
                    wrap: go.TextBlock.WrapFit,
                    editable: true
                },
            new go.Binding("text").makeTwoWay())
            // no ports, because no links are allowed to connect with a comment
        ));
        const myPalette =
        $(go.Palette, "myPaletteDiv",
            {
            contentAlignment: go.Spot.Center,
            nodeTemplate: myDiagram.nodeTemplate,
            scrollsPageOnFocus: false,
            nodeTemplateMap: myDiagram.nodeTemplateMap,  // share the templates used by myDiagram
            model: new go.GraphLinksModel([  // specify the contents of the Palette
                { category: "Start", text: "Start" },
                { text: "Step" },
                { text: "???", figure: "Diamond" },
                { category: "End", text: "End" },
                { category: "Comment", text: "Comment", textAlign: "center"}
            ]),
            "animationManager.isEnabled": false
        });
        myDiagram.linkTemplate =
        $(go.Link,
            {
              routing: go.Link.AvoidsNodes,
              curve: go.Link.JumpOver,
              corner: 5, toShortLength: 4,
              relinkableFrom: true,
              relinkableTo: true,
              reshapable: true,
              resegmentable: true,
              mouseEnter: function(e, link) { link.findObject("HIGHLIGHT").stroke = "rgba(30,144,255,0.2)"; },
              mouseLeave: function(e, link) { link.findObject("HIGHLIGHT").stroke = "transparent"; }
            },
            new go.Binding("points").makeTwoWay(),
            $(go.Shape,
              { isPanelMain: true, strokeWidth: 4, stroke: "transparent", name: "HIGHLIGHT" }),
            $(go.Shape,
              { isPanelMain: true, stroke: "gray", strokeWidth: 2 }),
            $(go.Shape,
              { toArrow: "standard", stroke: null, fill: "gray"}),
            $(go.Panel, "Auto",
              { visible: false, name: "LABEL", segmentIndex: 2, segmentFraction: 0.5},
              new go.Binding("visible", "visible").makeTwoWay(),
              $(go.Shape, "RoundedRectangle",
                { fill: "#FFFFFF", stroke: null }),
              $(go.TextBlock, "Yes",
                {
                  textAlign: "center",
                  font: "font-size: 0.5rem",
                  stroke: "#333333",
                  editable: true
                },
                new go.Binding("text").makeTwoWay())
            )
          );
        this.diagram = myDiagram;
        this.updateModel(this.modelData);
    },
    watch: {
        modelData: function(val) {
            this.updateModel(val);
        }
    },
    methods: {
        model: function() {
            return this.diagram.model;
        },
        updateModel: function(val) {
            if (val instanceof go.Model) {
                this.diagram.model = val;
            } else {
                var m = new go.GraphLinksModel()
                if (val) {
                    for (var p in val) {
                        m[p] = val[p];
                    }
                }
                this.diagram.model = m;
            }
        },
        updateDiagramFromData: function() {
            this.diagram.startTransaction();
            this.diagram.updateAllRelationshipsFromData();
            this.diagram.updateAllTargetBindings();
            this.diagram.commitTransaction("updated");
        }
    }
})

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
